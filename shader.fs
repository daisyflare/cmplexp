#version 330

in vec2 fragTexCoord;
in vec4 fragColor;

out vec4 finalColor;

uniform vec2 inverse;
uniform vec2 exp_coords;
uniform float max_x;
uniform float min_x;
uniform float max_y;
uniform float min_y;
uniform float width;
uniform float height;

const float radius = 0.0001f;
const float gridlines = 15.0f;

#define M_PI 3.1415926538

// Getting the complex number such that num * result = 1;
vec2 cinverse(vec2 num) {
    float scale = num.x * num.x + num.y * num.y;
    return vec2(num.x / scale, (-num.y) / scale);
}

vec2 cartesian(float r, float theta) {
    return vec2(r * cos(theta), r * sin(theta));
}

// Raising a real to a complex
vec2 rexpc(float base, vec2 exponent) {
    float scale = pow(base, exponent.x);
    float theta = exponent.y * log(abs(base));
    return cartesian(scale, theta);
}

vec2 cmul(vec2 a, vec2 b) {
    float real = a.x * b.x - a.y * b.y;
    float imag = a.y * b.x + a.x * b.y;
    return vec2(real, imag);
}

float cangle(vec2 c) {
    if (c.x == 0) {
        return M_PI / 2 * sign(c.y);
    } else if (c.x < 0){
        return M_PI + atan(c.y / c.x);
    } else {
        return atan(c.y / c.x);
    }
}

vec2 cexponent(vec2 base, vec2 exponent) {
    float r = sqrt(base.x * base.x + base.y * base.y);
    float theta = cangle(base);

    float scale = exp(-exponent.y * theta);
    float angle = exponent.x * theta;
    vec2 part1 = cartesian(scale, angle);

    vec2 part2 = rexpc(r, exponent);

    return cmul(part1, part2);
}
void main() {
    float ty = fragTexCoord.y;// / height;
    float tx = fragTexCoord.x;// / width;

    float dx = exp_coords.x - tx;
    float dy = exp_coords.y - ty;

    float ratio = height / width;

    if (dy*dy+ dx*dx/ratio <= radius) {
        finalColor = vec4(1.0, 0.0, 0.0, 1.0);
    } else {
        float sep_v = (max_y - min_y) / gridlines;
        float sep_h = (max_x - min_x) / gridlines;
        float real = mix(min_x, max_x, tx);
        float imag = -mix(min_y, max_y, ty); // It has to be negative because screen coordinates go downwards
                                            // while cartesian coordinates go upwards

        vec2 orig = cexponent(vec2(real, imag), inverse);
        // vec2 orig = vec2(real, imag);
#if 0
        float rg = (orig.y - min_y) / (max_y - min_y);
        float b = (orig.x - min_x) / (max_x - min_x);

        if (rg >= 0.0f && rg <= 1.0f && b >= 0.0f && b <= 1.0f){
            finalColor = vec4(rg, rg, b, 1.0f);
        } else {
            finalColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);
        }
#else
        if (mod(orig.x - min_x, sep_h) <= 0.01f || mod(orig.y - min_y, sep_v) <= 0.01f){
            finalColor = vec4(0.8, 0.8, 0.8, 1.0f);
        } else {
            finalColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);
        }
#endif
    }
}
