#include <stdio.h>
#include <math.h>
#include "raylib.h"
#include "raymath.h"

#define WIDTH 800
#define HEIGHT 800
bool done = false;

const float max_x =  5.0f;
const float min_x = -5.0f;
const float max_y =  5.0f;
const float min_y = -5.0f;

// Getting the complex number such that num * result = 1;
Vector2 cinverse(Vector2 num) {
    float scale = num.x * num.x + num.y * num.y;
    return CLITERAL(Vector2) {num.x / scale, (-num.y) / scale};
}

Vector2 cartesian(float r, float theta) {
    return CLITERAL(Vector2) {r * cos(theta), r * sin(theta)};
}

// Raising a real to a complex
Vector2 rexpc(float base, Vector2 exponent) {
    float scale = powf(base, exponent.x);
    float theta = exponent.y * log(fabs(base));
    return cartesian(scale, theta);
}

Vector2 cmul(Vector2 a, Vector2 b) {
    float real = a.x * b.x - a.y * b.y;
    float imag = a.y * b.x + a.x * b.y;
    return CLITERAL(Vector2) {real, imag};
}

int signf(float f) {
    return (f > 0.0f) - (f < 0.0f);
}

float cangle(Vector2 c) {
    if (c.x == 0) {
        return M_PI / 2 * (float) signf(c.y);
    } else if (c.x < 0){
        return M_PI + atan(c.y / c.x);
    } else {
        return atan(c.y / c.x);
    }
}

Vector2 cexponent(Vector2 base, Vector2 exponent) {
    float r = sqrt(base.x * base.x + base.y * base.y);
    float theta = cangle(base);

    float scale = expf(-exponent.y * theta);
    float angle = exponent.x * theta;
    Vector2 part1 = cartesian(scale, angle);

    Vector2 part2 = rexpc(r, exponent);

    return cmul(part1, part2);
}

Vector2 get_original_coord(int x, int y, Vector2 inverse) {
    float ty = (float) y / (float) HEIGHT;
    float tx = (float) x / (float) WIDTH;

    float real = Lerp(min_x, max_x, tx);
    float imag = -Lerp(min_y, max_y, ty); // It has to be negative because screen coordinates go downwards
                                          // while cartesian coordinates go upwards

    Vector2 here = CLITERAL(Vector2) { real, imag };
    return cexponent(here, inverse);
}
void rotate_screen(Vector2 exponent) {

    int exp_x = (int) Lerp(0, WIDTH, (exponent.x - min_x) / (max_x - min_x));
    int exp_y = (int) Lerp(0, HEIGHT, (exponent.y - min_y) / (max_y - min_y));
    int rad = 10;
    Vector2 inverse = cinverse(exponent);
    for (int y = 0; y < GetScreenHeight(); ++y) {
        for (int x = 0; x < GetScreenWidth(); ++x) {
            int dx = exp_x - x;
            int dy = exp_y - y;
            if (dx * dx + dy*dy <= rad*rad) {
                DrawPixel(x, y, CLITERAL(Color){255, 0, 0, 255});
            } else {
                Vector2 orig = get_original_coord(x, y, inverse);
                int rg = (int) Lerp(0, 255, (orig.y - min_y) / (max_y - min_y));
                int b = (int) Lerp(0, 255, (orig.x - min_x) / (max_x - min_x));
                if (rg > 0 && rg < 255 && b > 0 && b < 255){
                    DrawPixel(x, y, CLITERAL(Color){ rg, rg, b, 255 });
                }
            }
        }
    }
    done = true;
}

int main() {
    InitWindow(WIDTH, HEIGHT, "Complex exponentiation!");

    Shader shader = LoadShader(0, "shader.fs");

    RenderTexture2D target = LoadRenderTexture(GetScreenWidth(), GetScreenHeight());

    int inverse_loc = GetShaderLocation(shader, "inverse");

    int width_loc = GetShaderLocation(shader, "width");
    int height_loc = GetShaderLocation(shader, "height");

    int max_x_loc = GetShaderLocation(shader, "max_x");
    int min_x_loc = GetShaderLocation(shader, "min_x");
    int max_y_loc = GetShaderLocation(shader, "max_y");
    int min_y_loc = GetShaderLocation(shader, "min_y");

    int exp_coords_loc = GetShaderLocation(shader, "exp_coords");

    bool running = true;

    int fps = 60;
    SetTargetFPS(fps);

    float theta = 0.0f;
    float d_theta = M_PI / (float) fps / 4.0;
    float len = 1.0f;
    float scale = 0.9f;
    Vector2 exponent = cartesian(len, theta);
    exponent.y *= -1;
    float exp_coords[2] = {
        (exponent.x - min_x) / (max_x - min_x),
        (exponent.y - min_y) / (max_y - min_y)
    };
    printf("%f, %f\n", exp_coords[0], exp_coords[1]);
    Vector2 inverse = cinverse(exponent);

    bool moving = false;

    float width = (float) WIDTH;
    float height = (float) HEIGHT;

    SetShaderValue(shader, width_loc,            &width,   SHADER_UNIFORM_FLOAT);
    SetShaderValue(shader, height_loc,           &height,  SHADER_UNIFORM_FLOAT);

    SetShaderValue(shader, max_x_loc,            &max_x,   SHADER_UNIFORM_FLOAT);
    SetShaderValue(shader, min_x_loc,            &min_x,   SHADER_UNIFORM_FLOAT);
    SetShaderValue(shader, max_y_loc,            &max_y,   SHADER_UNIFORM_FLOAT);
    SetShaderValue(shader, min_y_loc,            &min_y,   SHADER_UNIFORM_FLOAT);

    SetShaderValue(shader, inverse_loc, (float*) &inverse, SHADER_UNIFORM_VEC2);
    SetShaderValue(shader, exp_coords_loc, exp_coords, SHADER_UNIFORM_VEC2);

    while (running && !WindowShouldClose()) {
        if (IsKeyPressed(KEY_ESCAPE)) {
            running = false;
        } else if (IsKeyReleased(KEY_SPACE)) {
            printf("%f\n", theta / M_PI);
            moving = !moving;
        } else if (IsKeyReleased(KEY_R)) {
            moving = false;
            len = 1.0f;
            theta = 0.0f;
        }
        if (IsMouseButtonReleased(0)) {
            int x = GetMouseX();
            int y = GetMouseY();
            Vector2 coord = get_original_coord(x, GetScreenHeight() - y, cinverse(cartesian(1.0f, theta)));
            printf("%f + %fi\n", coord.x, coord.y);
        }
        if (IsKeyReleased(KEY_LEFT)) {
            len *= scale;
        } else if (IsKeyReleased(KEY_RIGHT)) {
            len /= scale;
        }

        if (moving) {
            theta = fmodf((theta + d_theta), (float) (2*M_PI));
        }
        exponent = cartesian(len, theta);
        exponent.y *= -1;
        inverse = cinverse(exponent);
        float exp_coords[2] = {
            (exponent.x - min_x) / (max_x - min_x),
            (exponent.y - min_y) / (max_y - min_y)
        };
        SetShaderValue(shader, exp_coords_loc, exp_coords, SHADER_UNIFORM_VEC2);
        SetShaderValue(shader, inverse_loc, (float*) &inverse, SHADER_UNIFORM_VEC2);

        BeginTextureMode(target);
            ClearBackground(BLACK);
            DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), BLACK);
        EndTextureMode();

        BeginDrawing();
        ClearBackground(BLACK);
            BeginShaderMode(shader);
                DrawTextureEx(target.texture, CLITERAL(Vector2){0.0f, 0.0f}, 0.0f, 1.0f, WHITE);
            EndShaderMode();
            float font_size = width*0.04;
            float text_height = font_size / 0.85;
            DrawText(TextFormat("Exponent is %f + %fi", exponent.x, exponent.y), 0, height - 3*text_height, font_size, RAYWHITE);
            DrawText(TextFormat("%f < real part < %f", min_x, max_x), 0, height - 2*text_height, font_size, RAYWHITE);
            DrawText(TextFormat("%f < imaginary part < %f", min_y, max_y), 0, height - 1*text_height, font_size, RAYWHITE);
        EndDrawing();
    }
    UnloadShader(shader);
    UnloadRenderTexture(target);
    CloseWindow();
    return 0;
}
